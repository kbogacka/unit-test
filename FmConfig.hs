{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}
module FmConfig
    (
    -- * Global Settings
      settings

    -- * Game State
    , initialGameState
    , step
    , parseCommand
    ) where

import Control.Lens(view)
import Control.Lens.Setter((+=), (.=), (.~))
import Control.Monad.State(State)
import Data.ProtoLens(defMessage)
import Lens.Micro((&))

import FM.API(Settings(..))
import DefaultConfig(defaultSettings)
import Proto.Command(Command())
import qualified Proto.Command as C
import qualified Proto.Command_Fields as CF
import qualified ProtoWrapper as Protobuf

import Proto.Game(Game)
import qualified Proto.Game_Fields as GF

settings :: Settings
settings = defaultSettings

initialGameState :: IO Game
initialGameState = return $ defMessage
    & GF.testData .~ 0
    & GF.events .~ []

-- | Change the gamestate based on time passing
step :: Double -> State Game ()
step dt = return ()

-- | Interpret the command to show how it will change the gamestate
parseCommand :: Command -> State Game ()
parseCommand command = case Protobuf.commandData command of
    Just (C.Command'Inc increment) -> GF.testData += view CF.amount increment
    Just (C.Command'Nil _) -> return ()
    Nothing -> error "Invalid command type"
