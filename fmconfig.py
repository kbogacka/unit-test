from functools import partial
import sys
import os
import random
from client.main import Test, funcs_run_every_frame

sys.path.append(os.path.join(os.path.dirname(__file__), "client"))


def client_init():
    Test().test_word_command()


def run_every_frame(func):
    funcs_run_every_frame.append(func)


settings = {

    #  == File Locations
    # 'build path':   '../build/',

    # == Server Config
    # 'server host':  socket.gethostname(),
    # 'command port': 21701,
    # 'game port':    21702,
    # 'config port':  21703,

    # == Constants
    # 'update rate': 5,
    # 'max cmd size': 1024,

    # == Methods
    'client init': client_init,
    'render': lambda g: Test.render(g),
    'validate': lambda command: command,

    # == Engine-Specific Configuration
    'start game': lambda: None,
    'run every frame': run_every_frame,
}
