import proto
from command_pb2 import Word, Command
from functional_multiplayer import send_command
from time import sleep
import unittest

funcs_run_every_frame = []


class Test(unittest.TestCase):
    @staticmethod
    def render(game):
        pass

    def _do_frame(self):
        for f in funcs_run_every_frame:
            f()

    def test_word_command(self):
        def render(game):
            render_was_called = True
            self.assertEqual(game.test_data, 0)
        Test.render = render

        render_was_called = False

        big_word = Word()
        big_word.word = "HELLO I AM BIG"

        c = Command()
        c.word.CopyFrom(big_word)
        send_command(0, c)

        sleep(0.2)
        self._do_frame()
        self.assertTrue(render_was_called)
